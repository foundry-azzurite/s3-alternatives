# Rationale

Foundry [allows you to use Amazon AWS S3](https://foundryvtt.com/article/aws-s3/) for storing your Foundry media.

You may not like Amazon AWS S3 though. Fortunately, there are a ton of S3-compatible storage providers! The problem is that Foundry does not officially support them.

Fear not! Use any S3-compatible storage provider (like [Wasabi](https://wasabi.com/)) with this handy module.

# Install

Install the module with the module.json: [https://foundry-module.azzurite.tv/s3-alternatives/module.json](https://foundry-module.azzurite.tv/s3-alternatives/module.json)

# Setup

Fortunately, you can specify any options you want in the `awsConfig` you give Foundry. If you have any S3-compatible storage provider, the only thing you have to do is add an `endpoint` option to your `awsConfig`! Sample:

```json
{
	"endpoint": "https://s3.wasabisys.com",
	"accessKeyId": "<key>",
	"secretAccessKey": "<secret>"
}
```

The only remaining problem is that Foundry still points any URL to Amazon AWS. That's what this module fixes.

Install the module with the module.json: [https://foundry-module.azzurite.tv/s3-alternatives/module.json](https://foundry-module.azzurite.tv/s3-alternatives/module.json)

Then simply configure it (see the hints for the options):

![Settings Screenshot](/docs/settings.png)

And you're done!

# Contributing

All PRs welcome. Keep the formatting the same.
