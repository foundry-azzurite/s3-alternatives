import makeCustomS3 from './makeCustomS3/index.js';
import {registerSetting, getSetting} from './settings.js';
import {SETTINGS_KEYS} from './constants.js';
import {UserError, error, errorUser} from './messages.js';


Hooks.once('ready', () => {
	try {
		registerSettings();
		patchFilePicker();
	} catch (e) {
		error(e);
		errorUser(UserError)
	}
});


function registerSettings() {
	registerSetting(SETTINGS_KEYS.BASE_URL, {
		name: `baseUrl.name`,
		hint: `baseUrl.hint`,
		scope: `world`
	});
	registerSetting(SETTINGS_KEYS.BUCKET_REGIONS, {
		name: `bucketRegions.name`,
		hint: `bucketRegions.hint`,
		scope: `world`
	});
}

function patchFilePicker() {
	const origGetData = FilePicker.prototype.getData;
	FilePicker.prototype.getData = function getData(...args) {
		if (this.activeSource === "s3") {
			this.sources.s3.files = this.sources.s3.files.map(makeCustomS3);
		}
		return origGetData.apply(this, args);
	};
}
