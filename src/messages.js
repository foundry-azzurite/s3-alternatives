import {MODULE_NAME, NICE_MODULE_NAME} from './constants.js';
import localize from './localize.js';


export class UserError {
	msgKey;
	data;

	constructor(msgKey, ...data) {
		this.msgKey = `error.${msgKey}`;
		this.data = data;
	}

	toString() {
		return localize(this.msgKey, ...this.data);
	}
}

export function error(...msg) {
	console.error(MODULE_NAME, `|`, ...msg);
}

export function notifyError(msg) {
	ui.notifications.error(`${NICE_MODULE_NAME}: ${msg}`);
	const chatMessage = new ChatMessage({
		speaker: {alias: `${NICE_MODULE_NAME} Error`},
		content: `<span style=color:#800>${msg}</span> <br> This message will disappear on reload.`,
		whisper: [],
		timestamp: Date.now()
	});
	ui.chat.postOne(chatMessage, false);
}

const userErrors = new Set();
let lastError = null;

export function errorUser(userError) {
	userErrors.add(userError.toString());
	if (lastError !== null) clearTimeout(lastError);
	const id = lastError = setTimeout(() => {
		if (lastError !== id) return;

		userErrors.forEach((e) => {
			notifyError(e);
			error(e);
		});
		userErrors.clear();

		lastError = null;
	}, 500);
}
