import {MODULE_NAME} from './constants.js';
import localize from './localize.js';



export function registerSetting(key, data) {
	[`name`, `hint`].forEach((k) => {
		data[k] = localize(data[k]);
	});
	game.settings.register(MODULE_NAME, key, {
		config: true,
		type: String,
		default: ``,
		...data
	});
}

export function getSetting(key) {
	return game.settings.get(MODULE_NAME, key);
}
