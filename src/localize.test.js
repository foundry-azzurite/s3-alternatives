
import localize from './localize.js';

window.game = {
	i18n: {
		localize: () => {}
	}
};

window.Localization = {
	lang: `en`
};

describe(`localizePlaceholder`, () => {

	it(`should work without placeholder`, () => {
		const msg = `test`;
		spyOn(game.i18n, `localize`).and.returnValue(msg);

		expect(localize()).toBe(msg);
	});

	it(`should replace one placeholder`, () => {
		const msg = `{}`;
		spyOn(game.i18n, `localize`).and.returnValue(msg);
		const replacement = `test`;

		expect(localize(``, replacement)).toBe(replacement);
	});

	it(`should replace multiple placeholders`, () => {
		const msg = `1:{}, 2:{}, 3:{}`;
		spyOn(game.i18n, `localize`).and.returnValue(msg);

		expect(localize(``, `a`, `b`, `c`)).toBe(`1:a, 2:b, 3:c`);
	});

	it(`should return key when placeholders are wrong`, () => {
		const testKey = `key`;
		spyOn(game.i18n, `localize`).and.returnValue(``);

		expect(localize(testKey, 1)).toBe(testKey);
	});
});
