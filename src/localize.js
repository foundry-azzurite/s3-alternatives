import {MODULE_NAME} from './constants.js';
import {error} from './messages.js';

export default function localize(key, ...data) {
	const localized = game.i18n.localize(MODULE_NAME + `.` + key);
	if (data.length) {
		const parts = localized.split(`{}`);
		if (parts.length === 1) {
			error(`No placeholders (required: ${data.length}) found in ` +
				`translation key ${key} for language ${Localization.lang}`);
			return key;
		} else if ((parts.length - 1) !== data.length) {
			error(`Not the correct amount of placeholders (${data.length}) in translation key ${key}`);
			return key;
		} else {
			return parts.reduce((replaced, cur, idx) => {
				return replaced + cur + (data[idx] ? data[idx] : ``);
			}, ``)
		}
	}
	return localized;
}
