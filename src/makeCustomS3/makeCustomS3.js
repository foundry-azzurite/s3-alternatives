export default function makeCustomS3(settingsKeys, getSetting, messages) {
	const BUCKET_PLACEHOLDER = `%BUCKET%`;
	const REGION_PLACEHOLDER = `%REGION%`;

	function getBaseURL(bucket) {
		let baseUrl = getSetting(settingsKeys.BASE_URL);
		if (!baseUrl) return new messages.UserError(`baseUrl.missing`);

		if (baseUrl.indexOf(BUCKET_PLACEHOLDER) >= 0) {
			baseUrl = baseUrl.replace(BUCKET_PLACEHOLDER, bucket);
		}

		if (baseUrl.indexOf(REGION_PLACEHOLDER) >= 0) {
			const region = getRegion(bucket);
			if (region instanceof messages.UserError) return region;
			baseUrl = baseUrl.replace(REGION_PLACEHOLDER, region);
		}

		return baseUrl;
	}

	function createBucketRegions() {
		const bucketRegionConfigs = getSetting(settingsKeys.BUCKET_REGIONS);
		if (!bucketRegionConfigs) return new messages.UserError(`bucketRegion.notConfigured`);

		const bucketRegionConfig = bucketRegionConfigs.split(`,`);
		return bucketRegionConfig.reduce((bucketRegions, cur) => {
			if (bucketRegions instanceof messages.UserError) return bucketRegions;

			const bucketRegion = cur.split(`:`);
			if (bucketRegion.length !== 2) {
				return new messages.UserError(`bucketRegion.parse`, cur);
			}

			bucketRegions[bucketRegion[0]] = bucketRegion[1];
			return bucketRegions;
		}, {});
	}

	function getRegion(bucket) {
		const bucketRegions = createBucketRegions();
		if (bucketRegions instanceof messages.UserError) return bucketRegions;

		if (!bucketRegions.hasOwnProperty(bucket)) {
			return new messages.UserError(`bucketRegion.missing`, bucket);
		}

		return bucketRegions[bucket];
	}

	function getBucket(fileUrl) {
		const result = /https:\/\/([^.]+).s3-[^.]+.amazonaws.com/.exec(fileUrl);
		if (!result) {
			throw new Error(`Could not find bucket in ${fileUrl}`);
		}
		const [match, bucket] = result;
		return bucket;
	}


	return (fileUrl) => {
		const bucket = getBucket(fileUrl);
		const baseUrl = getBaseURL(bucket);
		if (baseUrl instanceof messages.UserError) {
			messages.errorUser(baseUrl);
			return fileUrl;
		}
		const path = decodeURI(new URL(fileUrl).pathname);
		return baseUrl + path;
	}
}
