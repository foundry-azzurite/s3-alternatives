
import defaultS3ToCustomConstructor from './makeCustomS3.js';
import {SETTINGS_KEYS} from '../constants.js';



function createFoundryS3Url(bucket = `bucket`) {
	return `https://${bucket}.s3-someregion.amazonaws.com`;
}

describe(`mapFile`, () => {

	let messagesSpy;
	function createMakeCustomS3(getSetting) {
		return defaultS3ToCustomConstructor(SETTINGS_KEYS, getSetting, messagesSpy);
	}

	beforeEach(() => {
		messagesSpy = jasmine.createSpyObj(`messages`, [`errorUser`, `UserError`]);
	});


	it(`should return same file when base url not configured`, () => {
		const testFn = createMakeCustomS3(() => undefined);
		const testUrl = createFoundryS3Url();

		expect(testFn(testUrl)).toBe(testUrl);
	});

	it(`should change base url when set without placeholders`, () => {
		const testBaseUrl = `baseUrl`;
		const testFile = `/test.png`;
		const testFn = createMakeCustomS3(() => testBaseUrl);

		expect(testFn(createFoundryS3Url() + testFile)).toBe(testBaseUrl + testFile);
	});

	it(`should replace bucket placeholder`, () => {
		const testFile = `/test.png`;
		const testBucket = `testBucket`;
		const testFn = createMakeCustomS3(() => `base%BUCKET%Url`);

		expect(testFn(createFoundryS3Url(testBucket) + testFile)).toBe(`base${testBucket}Url` + testFile);
	});

	it(`should replace region placeholder`, () => {
		const testFile = `/test.png`;
		const testRegion = `testRegion`;
		const testBucket = `testBucket`;
		const testFn = createMakeCustomS3((setting) => {
			switch (setting) {
				case SETTINGS_KEYS.BASE_URL: return `base%REGION%Url`;
				case SETTINGS_KEYS.BUCKET_REGIONS: return `${testBucket}:${testRegion}`;
			}
		});

		expect(testFn(createFoundryS3Url(testBucket) + testFile)).toBe(`base${testRegion}Url` + testFile);
	});

	it(`should parse bucket -> region mappings correctly`, () => {
		const testFn = createMakeCustomS3((setting) => {
			switch (setting) {
				case SETTINGS_KEYS.BASE_URL: return `%REGION%`;
				case SETTINGS_KEYS.BUCKET_REGIONS: return `b1:r1,b2:r2,b3:r3`;
			}
		});

		expect(testFn(createFoundryS3Url(`b1`))).toBe(`r1/`);
		expect(testFn(createFoundryS3Url(`b2`))).toBe(`r2/`);
		expect(testFn(createFoundryS3Url(`b3`))).toBe(`r3/`);
	});

	it(`should show error to user when base url not configured`, () => {
		const testFn = createMakeCustomS3(() => undefined);

		testFn(createFoundryS3Url());

		expect(messagesSpy.errorUser).toHaveBeenCalledWith(jasmine.any(messagesSpy.UserError));
	});

	it(`should show error to user when region mapping is not configured`, () => {
		const testFn = createMakeCustomS3((setting) => {
			switch (setting) {
				case SETTINGS_KEYS.BASE_URL: return `%REGION%`;
				case SETTINGS_KEYS.BUCKET_REGIONS: return null;
			}
		});

		testFn(createFoundryS3Url());

		expect(messagesSpy.errorUser).toHaveBeenCalledWith(jasmine.any(messagesSpy.UserError));
	});

	it(`should have the same path as before`, () => {
		const testPath = `/test/path.png`;
		const testBaseUrl = `https://test`;
		const testFn = createMakeCustomS3(() => testBaseUrl);

		expect(testFn(createFoundryS3Url() + testPath)).toBe(testBaseUrl + testPath);
	});
});
