import {getSetting} from '../settings.js';
import {SETTINGS_KEYS} from '../constants.js';
import builder from './makeCustomS3.js';
import * as logger from '../messages.js';

const makeCustomS3 = builder(SETTINGS_KEYS, getSetting, logger);

export default makeCustomS3;
