export const MODULE_NAME = `s3-alternatives`;
export const NICE_MODULE_NAME = `S3 Alternatives`;

export const SETTINGS_KEYS = {
	BASE_URL: `s3.baseUrl`,
	BUCKET_REGIONS: `s3.bucketRegions`
};
